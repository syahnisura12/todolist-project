import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LoginPage from "./component/LoginPage";
import SignUp from "./component/SignUp";
import HomePage from "./component/HomePage";
import ImportantPage from "./component/ImportantPage";

class App extends React.Component {
  state = {
    todos: []
  };
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/signup" component={SignUp} />
          <Route path="/loginpage" component={LoginPage} />
          <Route path="/homepage" component={HomePage} />
          <Route path="/importantpage" component={ImportantPage} />
        </Switch>
      </Router>
    );
  }
}

export default App;
