import React, { Component } from "react";
import axios from "axios";
import { Link, withRouter } from "react-router-dom";

export class ButtonLoginPage extends Component {
  state = {
    email: "",
    password: ""
  };
  handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await axios.post(
        "https://awesome-project-glints.herokuapp.com/api/v1/auth/login",
        this.state
      );
      console.log(response.data.data.token);
      localStorage.setItem("token", response.data.data.token);
      this.props.history.push("/homepage");
    } catch (err) {
      console.log(err.response.data);
    }
  };
  handleInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    console.log(this.state);
    return (
      <div>
        <button className="button-container">f</button>
        <button className="button-container-1">G+</button>
        <button className="button-container-2">in</button>
        <p className="section-2">or use your email account</p>
        <form onSubmit={this.handleSubmit}>
          <input
            onChange={this.handleInput}
            type="text"
            name="email"
            placeholder="email"
            className="input-1"
          />
          <br />
          <input
            onChange={this.handleInput}
            type="password"
            name="password"
            placeholder="password"
            className="input-1"
          />

          <button className="button-signup">SIGN IN</button>
        </form>
        <a href="#" className="link-forgot">
          forgot password ?
        </a>
      </div>
    );
  }
}

export default withRouter(ButtonLoginPage);
