import React from "react";
import "../styles/HomePage.scss";
import { Link } from "react-router-dom";

class Navigation extends React.Component {
  render() {
    return (
      <div className="navigation">
        <div className="wrapper">
          <h5>Todos</h5>
          <Link to="/loginpage">
            <button>Sign Out</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Navigation;
