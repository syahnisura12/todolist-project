import React from "react";
import Navigation from "./Navigation";
import Sidebar from "./Sidebar";
import FormTodoList from "./FormTodoList";

export class HomePage extends React.Component {
  render() {
    return (
      <div className="container-todo">
        <Navigation />
        <div className="wrapper">
          <Sidebar />
          <div className="todolist">
            <FormTodoList />
          </div>
        </div>
      </div>
    );
  }
}

export default HomePage;
