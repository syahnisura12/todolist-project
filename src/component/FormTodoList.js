import React from "react";
import "../styles/FormTodoList.scss";
import axios from "axios";

let baseUrl = "https://awesome-project-glints.herokuapp.com/api/v1";
let token = localStorage.getItem("token");

class TodoList extends React.Component {
  state = {
    todos: [],
    title: ""
  };

  changeTitle = e => {
    this.setState({
      title: e.target.value
    });
  };

  getTodos = async () => {
    console.log(token);
    try {
      let response = await axios.get(`${baseUrl}/tasks`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        }
      });
      this.setState({
        todos: response.data.data.docs
      });
    } catch (error) {
      console.log(error);
    }
  };

  addTodos = async () => {
    const newTodo = {
      title: this.state.title,
      dueDate: "2020-02-29",
      importanceLevel: 1
    };
    try {
      await axios.post(`${baseUrl}/tasks`, newTodo, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        }
      });
      await this.getTodos();
      this.setState({
        title: ""
      });
    } catch (error) {
      console.log(error);
    }
  };

  deleteTodos = async id => {
    try {
      await axios.delete(`${baseUrl}/tasks?id=${id}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        }
      });
      this.setState({
        todos: this.state.todos.filter(item => item._id !== id)
      });
    } catch (error) {
      console.log(error);
    }
  };

  importantTodos = async (id, level) => {
    const newTodo = {
      importanceLevel: level === 1 ? 3 : 1
    };
    try {
      await axios.put(`${baseUrl}/tasks?id=${id}`, newTodo, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        }
      });
      await this.getTodos();
      this.setState({
        title: ""
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.getTodos();
  }

  render() {
    let lists = this.state.todos.map(item => (
      <li key={item.id} className="form-todo">
        <label>
          <input type="checkbox" />
          <span>{item.title}</span>
          {/* <span>{item.dueDate}</span> */}
        </label>
        <div>
          <i
            class={
              item.importanceLevel === 1
                ? "far fa-star m-10"
                : "fas fa-star m-10"
            }
            onClick={() => this.importantTodos(item._id, item.importanceLevel)}
          ></i>
          <i class="fas fa-pencil-alt m-10"></i>
          <i
            class="fas fa-trash m-10"
            onClick={() => this.deleteTodos(item._id)}
          ></i>
        </div>
      </li>
    ));
    return (
      <div className="todolist">
        <div className="input-todo">
          <input
            onChange={this.changeTitle}
            value={this.state.title}
            type="text"
            name="title"
            placeholder="add task.."
            className="todolist-text"
          />
          <button onClick={this.addTodos}>+</button>
        </div>
        <div className="list">
          <h3 className="task-todo-1">Task</h3>
          <h3 className="task-todo">Important</h3>
        </div>
        <div className="border">
          <ul className="text-todo">{lists}</ul>

          {/* <ul>
            
            <li>
              <label>
                <input type="checkbox" />
                <span>Writing a new book</span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" />
                <span>Writing a new book</span>
              </label>
            </li>
          </ul> */}
          {/* <form>
            <input type="checkbox" />
            <h1 className="readingbook">Reading book</h1>
            <h1>Writing new blog</h1>
            <h1>praying</h1>
            <button>*</button>
          </form> */}
        </div>
      </div>
    );
  }
}

export default TodoList;
