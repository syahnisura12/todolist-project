import React from "react";
import "../styles/Button.scss";
import axios from "axios";
import { Link, withRouter } from "react-router-dom";

class FormRegistrasi extends React.Component {
  state = {
    fullname: "",
    email: "",
    password: "",
    password_confirmation: ""
  };
  handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await axios.post(
        "https://awesome-project-glints.herokuapp.com/api/v1/users",
        this.state
      );
      console.log(response.data.data.token);
      localStorage.setItem("token", response.data.data.token);
      this.props.history.push("/homepage");
    } catch (err) {
      console.log(err.response.data);
    }
  };

  handleInput = e => {
    // console.log(e.target.name);
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  render() {
    console.log(this.state);
    return (
      <div>
        <button className="button-container">f</button>
        <button className="button-container-1">G+</button>
        <button className="button-container-2">in</button>
        <p className="section-2">or use your email for registration</p>
        <form onSubmit={this.handleSubmit}>
          <input
            onChange={this.handleInput}
            type="text"
            name="fullname"
            placeholder="name"
            className="input-1"
          />
          <br />
          <input
            onChange={this.handleInput}
            type="text"
            name="email"
            placeholder="email"
            className="input-1"
          />
          <br />
          <input
            onChange={this.handleInput}
            type="password"
            name="password"
            placeholder="password"
            className="input-1"
          />
          <input
            onChange={this.handleInput}
            type="password"
            name="password_confirmation"
            placeholder="confirm password"
            className="input-1"
          />

          <button className="button-signup">SIGN UP</button>
        </form>
      </div>
    );
  }
}

export default withRouter(FormRegistrasi);
