import React, { Component } from "react";
import Sidebar from "./Sidebar";
import Navigation from "./Navigation";
import ImportantComponent from "./ImportantComp/ImportantComponent";
import { Link } from "react-router-dom";
// scss

import "../styles/HomePage.scss";

export class ImportantPage extends Component {
  render() {
    return (
      <div>
        <div className="container-todo">
          <Navigation />
          <div className="wrapper">
            <Sidebar />
            <div className="todolist">
              <ImportantComponent />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ImportantPage;
