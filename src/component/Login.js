import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Login extends Component {
  render() {
    return (
      <div>
        <form>
          <Link to="/signup">
            <button className="button-signin">SIGN UP</button>
          </Link>
        </form>
      </div>
    );
  }
}

export default Login;
