import React from "react";
import Register from "./Register";

import "../styles/Button.scss";

import FormRegistrasi from "./FormRegistrasi";

class SignUp extends React.Component {
  state = {};
  render() {
    return (
      <div className="sign-up">
        <div className="app">
          <h5 className="header-todos">Todos</h5>
          <h1 className="heading">Welcome Back!</h1>
          <p className="section">
            To keep connected with us please <br />
            login with your personal info
          </p>
          <Register />
        </div>
        <div className="back-ground-1">
          <h1>Created Account</h1>
          <FormRegistrasi />
        </div>
      </div>
    );
  }
}

export default SignUp;
