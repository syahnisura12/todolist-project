import React from "react";
import { Link } from "react-router-dom";

import "../styles/SidebarHomePage.scss";

class Sidebar extends React.Component {
  render() {
    return (
      <div className="sidebar">
        <div className="profile">
          <div className="avatar">
            <img src="http://unsplash.it/100/100" alt="foto" />
          </div>
          <h3>Glints X Binar</h3>
        </div>
        <ul className="category">
          <li>
            <a href="#">My Day</a>
          </li>
          <li>
            <Link to="/importantpage">Important</Link>
          </li>
          <li>
            <a href="#">Completed</a>
          </li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
