import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../styles/Button.scss";

class Register extends Component {
  render() {
    return (
      <div>
        <form>
          <Link to="/loginpage">
            <button className="button-signin">SIGN IN</button>
          </Link>
        </form>
      </div>
    );
  }
}

export default Register;
