import React, { Component } from "react";
import axios from "axios";

let baseUrl = "https://awesome-project-glints.herokuapp.com/api/v1";
let token = localStorage.getItem("token");

export class ImportantComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [],
      title: "",
      dataImportance: []
    };
  }

  componentDidMount() {
    this.getTodos();
  }

  getTodos = async () => {
    console.log(token);
    try {
      let response = await axios.get(`${baseUrl}/tasks`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token
        }
      });
      console.log(response.data.data);
      this.setState({
        todos: response.data.data.docs
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const dataType = this.state.todos.map(showImportance => {
      console.log(showImportance);
      if (showImportance.importanceLevel === 3) {
        return (
          <li>
            <label>
              <input type="checkbox" />
              <span> {showImportance.title} </span>
            </label>
          </li>
        );
      } else {
        return console.log("opps!");
      }
    });

    return (
      <div className="todolist">
        <ul>{dataType}</ul>
      </div>
    );
  }
}

export default ImportantComponent;
