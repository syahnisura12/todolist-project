import React, { Component } from "react";
import Login from "./Login";
import ButtonLoginPage from "./ButtonLoginPage";

export class LoginPage extends Component {
  render() {
    return (
      <div className="sign-up">
        <div className="app">
          <h5 className="header-todos">Todos</h5>
          <h1 className="heading">Hello Friend!</h1>
          <p className="section">
            Enter your personal details and start your journey with us
          </p>
          <Login />
        </div>
        <div className="back-ground-1">
          <h1>Sign in to Task Manager</h1>
          <ButtonLoginPage />
        </div>
      </div>
    );
  }
}

export default LoginPage;
